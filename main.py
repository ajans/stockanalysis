import web_scraper
import analysis

input = input("Enter a stock symbol: ")

html = web_scraper.get_html(input)
headlines, sources, info, links = web_scraper.get_news(html)

df = web_scraper.convert_to_df(headlines, sources, info, links)

df = analysis.get_sentiment(df)

#print(df.head())
print("Mean sentiment: ", analysis.mean_sentiment(df))
print("Sentiment: ", analysis.judge_from_mean(analysis.mean_sentiment(df)))
#analysis.plot_sentiment(df)
#analysis.scatter_sentiment(df)
#print(analysis.get_news_sentiment_df(input))
#analysis.get_news_sentiment_df_csv(input)
