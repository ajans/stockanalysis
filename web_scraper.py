from bs4 import BeautifulSoup
from urllib.request import urlopen as uReq, Request
import requests
import pandas as pd
import re
import matplotlib.pyplot as plt
# nltk.download('punkt')
# nltk.download('stopwords')
# nltk.download('wordnet')
def get_html(input_ticker):
    if input_ticker == "":
        print("No input")
        return
    
    ticker = input_ticker
    url = 'https://www.finance.yahoo.com/quote/{}?p={}&.tsrc=fin-srch'.format(ticker, ticker)
    #print(url)
    req = Request(url, headers={'User-Agent': 'Mozilla/5.0'})
    response = requests.get(url)
    if not response.ok:
        print('Server responded:', response.status_code)
        raise Exception('Failed to fetch')
    
    #read the data from the URL and store it in a variable
    html_data = BeautifulSoup(response.text, 'html.parser')
    #print(html_data)
    return html_data

def get_news(html_data):
    news = html_data.find_all('div', {'class': 'Ov(h) Pend(44px) Pstart(25px)'})
    headlines = []
    info = []
    sources = []
    links = []
    dates = []
    for i in news:
        sources.append(i.find('div').text)
        headlines.append(i.find('a').text)
        info.append(i.find('p').text)
        links.append(i.find('a')['href'])

    return headlines, sources, info, links

def convert_to_df(headlines, sources, info, links):
    df = pd.DataFrame()
    df['Headline'] = headlines
    df['Source'] = sources
    df['Info'] = info
    df['Links'] = links
    return df



def get_news_sentiment(input_ticker):
    html = get_html(input_ticker)
    headlines, sources, info, links = get_news(html)
    df = convert_to_df(headlines, sources, info, links)
    df = get_sentiment(df)
    return df

def get_news_sentiment_df(input_ticker):
    df = get_news_sentiment(input_ticker)
    return df

def get_news_sentiment_df_csv(input_ticker):
    df = get_news_sentiment(input_ticker)
    df.to_csv('news_sentiment.csv', index=False)
    return df
