### StockAnalyzer

Analyze stocks for positive, neagtive or neutral sentiment using news data. The news data is collected by web scraping from Yahoo finance with the help of BeautifulSoup. The articles are collected using their html tags and saved to a pandas dataframe for storage and analysis. The sub categories of data include headlines, source, date and subheading. VADER is used for analyzing the data. VADER is a sentiment analysis tool that is part of the Natural Language Toolkit (NLTK) library. VADER is suited for analyzing sentiments expressed in texts, such as tweets, reviews and comments. So it has been adapted to classify sentiments across various news articles. 

### INSTALL DEPENDENCIES

pip install -r requirements.txt

### RUN

python main.py
