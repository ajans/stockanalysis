import matplotlib.pyplot as plt
import nltk
import pandas as pd
nltk.download('vader_lexicon')
from nltk.sentiment.vader import SentimentIntensityAnalyzer

def get_sentiment(df):
    sia = SentimentIntensityAnalyzer()
    df['Compound'] = [sia.polarity_scores(v)['compound'] for v in df['Info']]
    df['Negative'] = [sia.polarity_scores(v)['neg'] for v in df['Info']]
    df['Neutral'] = [sia.polarity_scores(v)['neu'] for v in df['Info']]
    df['Positive'] = [sia.polarity_scores(v)['pos'] for v in df['Info']]
    return df

def mean_sentiment(df):
    mean = df['Compound'].mean()
    return mean

def judge_from_mean(mean):
    if mean >= 0:
        return "Positive"
    elif mean <= 0:
        return "Negative"
    else:
        return "Neutral"
    
    
    
def plot_sentiment(df):
    df.plot(kind='bar', y='Compound', x='Source', color='blue')
    plt.title('Sentiment Analysis')
    plt.xlabel('News Source')
    plt.ylabel('Sentiment')
    plt.show()

def scatter_sentiment(df):
    df.plot(kind='scatter', y='Compound', x='Source', color='blue')
    plt.title('Sentiment Analysis')
    plt.xlabel('News Source')
    plt.ylabel('Sentiment')
    plt.show()